

$(window).load(function() {
    $("#owl-demo").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        mouseDrag: false,
        pagination: false,
        navigationText: ["<i class='icon-left-open-big'></i>","<i class='icon-right-open-big'></i>"]

    });
    $("#owl-demo2").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        mouseDrag: false,
        pagination: false,
        navigationText: ["<i class='icon-left-open-big'></i>","<i class='icon-right-open-big'></i>"]

    });

    $(".collapse__elem-title a").click(function () {
        $(this).parent().hide().next().show('slow');
        return false;
    });

    // input type ONLY number
    jQuery.fn.ForceNumericOnly =
        function()
        {
            return this.each(function()
            {
                $(this).keydown(function(e)
                {
                    var key = e.charCode || e.keyCode || 0;
                    // sucess backspace, tab, delete, arrows, numbers
                    return (
                    key == 8 ||
                    key == 9 ||
                    key == 46 ||
                    key == 190 ||
                    (key >= 37 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
                });
            });
        };
    $(".input__number-js").ForceNumericOnly();


    //busket delete function
    $('.busket__del-js a').click(function(){
        $(this).parent().parent().fadeOut('slow');
        return false;
    });

    // mobile filter
    $('.filter__title-js').click(function(){
        $(this).toggleClass('active').next().slideToggle();
        return false;
    });

    // tel
    $("[name=phone]").mask("+7 (999) 999-9999");


    // max-height
    var mh = 0;
    $(".product-item__inner").each(function () {
        var h_block = parseInt($(this).height());
        if(h_block > mh) {
            mh = h_block;
        };
    });
    $(".product-item__inner").height(mh);


    // slider
    var sync1 = $("#owl-card");
    var sync2 = $("#owl-card-thumb");
    sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: false,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });

    sync2.owlCarousel({
        items : 7,
        itemsDesktop      : [1199,7],
        itemsDesktopSmall     : [979,5],
        itemsTablet       : [768,4],
        itemsMobile       : [479,3],
        pagination:false,
        navigation: true,
        navigationText: ["<i class='icon-left-open-big'></i>","<i class='icon-right-open-big'></i>"],
        responsiveRefreshRate : 100,
        afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
        }
    });
    function syncPosition(el){
        var current = this.currentItem;
        $("#owl-card-thumb")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
        if($("#owl-card-thumb").data("owlCarousel") !== undefined){
            center(current)
        }
    }
    $("#owl-card-thumb").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
    });
    function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
            if(num === sync2visible[i]){
                var found = true;
            }
        }
        if(found===false){
            if(num>sync2visible[sync2visible.length-1]){
                sync2.trigger("owl.goTo", num - sync2visible.length+2)
            }else{
                if(num - 1 === -1){
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if(num === sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
            sync2.trigger("owl.goTo", num-1)
        }
    }
});
